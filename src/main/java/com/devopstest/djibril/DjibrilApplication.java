package com.devopstest.djibril;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DjibrilApplication implements CommandLineRunner {

	public static final Logger logger = LoggerFactory.getLogger(DjibrilApplication.class);



	public static void main(String[] args) {
		SpringApplication.run(DjibrilApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		logger.info("djibril devops");
	}
}
